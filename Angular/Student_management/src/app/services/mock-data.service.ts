import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from '../models/user.model';

export class MockDataService implements InMemoryDbService {
  createDb() {
    const users: User[] = [
      {
        id: 1, username: 'ABC', email: 'abc@argus.com', gender: 'M',
        designation: 'UI designer', known_techs: ['Java', 'PHP', 'Python']
      },
      {
        id: 2, username: 'DEF', email: 'def@argus.com', gender: 'M',
        designation: 'UI designer', known_techs: ['Angular', 'NodeJs']
      },
      {
        id: 3, username: 'GHI', email: 'ghi@argus.com', gender: 'M',
        designation: 'Business Analyst', known_techs: ['Java', 'Python']
      },
      {
        id: 4, username: 'JKL', email: 'jkl@argus.com', gender: 'M',
        designation: 'Business Analyst', known_techs: ['Angular', 'MongoDb', 'NodeJs']
      },
      {
        id: 5, username: 'MNO', email: 'mno@argus.com', gender: 'F',
        designation: 'Business Analyst', known_techs: ['Angular', 'NodeJs']
      },
      {
        id: 6, username: 'OPQ', email: 'opq@argus.com', gender: 'F',
        designation: 'Business Analyst', known_techs: ['Java', 'PHP', 'Python']
      },
      {
        id: 7, username: 'RST', email: 'rst@argus.com', gender: 'F',
        designation: 'Programmer Analyst', known_techs: ['Angular', 'MongoDb', 'NodeJs']
      },
      {
        id: 8, username: 'UVW', email: 'uvw@argus.com', gender: 'F',
        designation: 'Programmer Analyst', known_techs: ['Java', 'PHP', 'Python']
      },
      {
        id: 9, username: 'XYZ', email: 'xyz@argus.com', gender: 'M',
        designation: 'Programmer Analyst', known_techs: ['Angular', 'NodeJs']
      },
      {
        id: 10, username: 'QWE', email: 'qwe@argus.com', gender: 'F',
        designation: 'Programmer Analyst', known_techs: ['Java', 'Python']
      },
    ];
    return { users };
  }
}
