import { NgModule } from '@angular/core';
import {
  AlertModule,
  ModalModule,
  PaginationModule
} from 'ngx-bootstrap';

@NgModule({
  imports: [
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  declarations: [],
  exports: [
    AlertModule,
    ModalModule,
    PaginationModule
  ]
})
export class NgxBsModule { }
