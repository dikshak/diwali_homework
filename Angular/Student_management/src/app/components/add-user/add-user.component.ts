import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  designations = [
    'Programmer Analyst',
    'UI designer',
    'Business Analyst'
  ];
  knownTechs = [
    'Java',
    'Python',
    'Angular',
    'NodeJs',
    'PHP',
    'MongoDb',
    'C#'
  ];
  isFormSubmitted = false;
  editMode = false;
  userid: number;
  retrieveError = false;

  constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService) {

    this.userid = +this.route.snapshot.paramMap.get('id');
    this.editMode = this.userid !== 0;
    this.createForm();
  }

  ngOnInit() {
    this.setTitle();
    if (this.editMode) {
      this.setUserData();
    }
  }

  setTitle(): void {
    if (this.editMode) {
      this.titleService.setTitle('Edit user');
    } else {
      this.titleService.setTitle('Add user');
    }
  }

  createForm(): void {
    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3)]],
      designation: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      gender: ['', Validators.required],
      known_techs: ['', Validators.required]
    });
  }

  setUserData(): void {
    this.userService.getUserById(this.userid)
      .subscribe(user => {
        this.userForm.setValue({
          'username': user.username,
          'email': user.email,
          'designation': user.designation,
          'gender': user.gender,
          'known_techs': user.known_techs
        });
      }, err => {
        this.retrieveError = true;
      });
  }

  get username() { return this.userForm.get('username'); }
  get email() { return this.userForm.get('email'); }
  get designation() { return this.userForm.get('designation'); }
  get gender() { return this.userForm.get('gender'); }
  get known_techs() { return this.userForm.get('known_techs'); }

  submitForm(user: User): void {
    if (this.userForm.valid) {
      if (this.editMode) {
        user.id = this.userid;
        this.userService.updateUser(user)
        .subscribe(data => { this.isFormSubmitted = true; },
          err => {console.log(err); this.isFormSubmitted = null; });
      } else {
        this.userService.addUser(user)
          .subscribe(data => { this.isFormSubmitted = true; },
            err => { this.isFormSubmitted = null; });
      }
    }
  }
}
