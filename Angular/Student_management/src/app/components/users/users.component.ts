import { Component, OnInit, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  userToBeDisplayed: User[];
  isDataAvailable = false;
  alertMessage = {
    display: false,
    type: '',
    content: ''
  };
  modalRef: BsModalRef;
  userToBeDeleted = -1;
  currentPage = 1;
  itemsPerPage = 7;

  constructor(
    private titleService: Title,
    private userService: UserService,
    private modalService: BsModalService,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.titleService.setTitle('Users');
    this.loadUsers();
  }

  loadUsers(): void {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
      this.setDisplayData(this.currentPage);
      this.isDataAvailable = true;
      this.displayAlert('info', 'Users successfuly fetched!');
    }, err => {
      this.displayAlert('danger', 'Problem fetching users, try again later!');
    });
  }

  deleteUser(): void {
    this.userService.deleteUser(this.userToBeDeleted).subscribe(() => {
      this.users = this.users.filter(user => user.id !== this.userToBeDeleted);
      this.setDisplayData(this.currentPage);
      this.displayAlert('warning', 'User deleted successfully!');
    }, err => {
      this.displayAlert('danger', 'Error deleting user!');
    });
    this.modalRef.hide();
  }

  deleteUserConfirm(template: TemplateRef<any>, userid: number) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
    this.userToBeDeleted = userid;
  }

  decline(): void {
    this.modalRef.hide();
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.setDisplayData(this.currentPage);
  }

  setDisplayData(page: number): void {
    this.userToBeDisplayed = this.users.slice((page - 1) * this.itemsPerPage,
      page * this.itemsPerPage);
    this.cdr.detectChanges();
  }

  displayAlert(type: string, content: string): void {
    this.alertMessage.type = type;
    this.alertMessage.content = content;
    this.alertMessage.display = true;
    setTimeout(() => {
      this.alertMessage.display = false;
    }, 3000);
  }

  getGenderCountObject(): any {
    const maleCount = this.users.filter(user => user.gender === 'M').length;
    const femaleCount = this.users.length - maleCount;
    return {
      'maleCount': maleCount,
      'femaleCount': femaleCount
    };
  }

}
