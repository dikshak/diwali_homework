export interface User {
    id: number;
    username: string;
    email: string;
    gender: string;
    designation: string;
    known_techs: string[];
}
