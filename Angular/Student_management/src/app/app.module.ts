import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { MockDataService } from './services/mock-data.service';
import { ChartModule } from 'angular-highcharts';

import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { UserService } from './services/user.service';

import { NgxBsModule } from './modules/ngx-bs/ngx-bs.module';
import { MainComponent } from './components/main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    AddUserComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(MockDataService, { dataEncapsulation: false }),
    NgxBsModule,
    ChartModule
  ],
  providers: [
    Title,
    UserService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
