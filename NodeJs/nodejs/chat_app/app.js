#!/usr/bin/env supervisor

const express = require('express'),
  cors = require('cors'),
  bodyParser = require('body-parser'),
  app = express(),
  helmet = require('helmet'),
  common = require('./common'),
  config = common.config();

app.use(helmet());
require('dotenv').config();

// Set up mongoose connection
// DEV_DB_URL = mongodb://localhost:27017/flyt_live
// DEV_DB_URL = mongodb://hiteshpr:hitesh12345@ds119663.mlab.com:19663/flyt_live

const mongoose = require('mongoose');
mongoose.connect(config.DB_URL);
console.log("DB URL: " + config.DB_URL);

mongoose.Promise = global.Promise;

const db = mongoose.connection;

//db.on('error', console.error.bind(console, 'MongoDB connection error:'));

db.on('error', function () {
  console.error('MongoDB connection error:');
});
// mongoose setup completed

// CORS code

app.use(cors());
// CORS ends

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

// Redis Code
let redis = require('redis');
let redisClient = redis.createClient(); // create a Redis Client

redisClient.on('ready', function () {
  console.log("Redis is ready"); // If redis successfully started, console log this
});

redisClient.on('error', function () {
  console.log("Error in Redis"); //     if redis successfully started, console log this
});

// import routes and JWT library
const jwt = require("jsonwebtoken"),
  adminRoute = require("./routes/user_details.route"),
  auth = require("./auth"),
  loginRoute = require("./routes/login.route"),
  userRoute = require("./routes/user.route"),
  drone_details = require("./routes/drone_details.route");
geofenceRoute = require("./routes/geofence.route");
missionRoute = require("./routes/mission_details.route");
checklistRoute = require("./routes/checklist_details.route");
msgCode=require("./msgCode.json");

// define routes that do not require authentication here
app.use("/login", loginRoute);

/* middleware to test authentication of the user. */
app.use((req, res, next) => {
  // check header or url parameters or post parameters for token
  var header = JSON.parse(req.headers["authorization"]);
  var token = header.token;

  if (token) {
    //Decode the token
    jwt.verify(token, auth.secretCode, (err, decod) => {
      if (err) {
        res.status(403).json({
          'code':msgCode.token_expired.code,  'message': msgCode.token_expired.msg
        });
      } else {
        //If decoded then call next() so that respective route is called.
        res.locals.userId = decod.id;
        res.locals.emailId = decod.email;
        checkAdmin(decod.isAdmin, header.refreshToken)
          .then(data => {
            if (data) {
              res.locals.isAdmin = true;
              next();
            }
          })
          .catch(err => {
            res.locals.isAdmin = false;
            next();
          });
      }
    });
  } else {
    res.status(403).json({
      'code':msgCode.no_token.code,  'message': msgCode.no_token.msg
    });
  }
});
/* middleware to test authenticatuin ends */

/* define routes that require authentication here */
app.use("/admin", adminRoute);
app.use("/drone_details", drone_details);
app.use("/user", userRoute);
app.use("/geofence", geofenceRoute);
app.use("/mission", missionRoute);
app.use("/checklist", checklistRoute);

// post on which node server would run.
const port = 1234;

app.listen(port, () => {
  console.log("Server is up and running on port number " + port);
});

// function to test Admin authentication
async function checkAdmin(isAdmin, refToken) {
  return new Promise((resolve, reject) => {
    if (isAdmin) {
      redisClient.get(refToken, function(err, reply) {
        if (reply == "admin") {
          resolve(true);
        } else {
          reject(false);
        }
      });
    } else {
      reject(false);
    }
  });
}
