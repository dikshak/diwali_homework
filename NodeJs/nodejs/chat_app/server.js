
// body parser is used to perse form data
var bodyParser=require('body-parser')
var express= require('express');
var app= express();
var http = require('http').Server(app)
var io= require('socket.io')(http)
var mongoose= require('mongoose')

app.use(express.static(__dirname))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))

var dbUrl='mongodb://dikshakondekar:Sawli0209@ds061288.mlab.com:61288/flyt_live'

/**
 *jsDoc
  this is a mode to define message format
 * @function Send_Message
 * @param  {type} 'Message' message send by user
 * @param  {type} 'name' specifies user name
 * @return {type} {description}
 */
var Message=mongoose.model('Message',{
    name: String,
    message: String
})


 /** @namespace */
var messages=[
    /**
         * Repeat <tt>str</tt> several times.
         * @param {string} str The string to repeat.
         * @param {number} [times=1] How many times to repeat the string.
         * @returns {string}
         */

    {name:'dik', message:'hi'},
    {name:'dk', message:'hello'}

]

/**
 * This function fetch all messages
 * @param  {} '/messages'
 * @param  {} (req
 * @param  {} res
 */
app.get('/messages',(req,res)=>{
    res.send(messages )
})

/**
 * @param  {} '/messages'
 * @param  {} (req
 * @param  {} res
 */
app.post('/messages',(req,res)=>{

    var message =new Message(req.body)

    message.save((err)=>{
        if(err){
            sendStatus(500)
        }

        console.log(req.body)
    messages.push(req.body)
    io.emit('message',req.body)
    res.sendStatus(200)
    })
    
})

io.on('connection', (socket)=>{
    console.log('user connected')
})

mongoose.connect(dbUrl, {useMongoClient:true}, (err)=>{
    console.log('mongo db connect')
})

var server= http.listen(3000,()=>{
    console.log('server is listenin on port',server.address().port)
})