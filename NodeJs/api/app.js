const express = require("express"),
  cors = require("cors"),
  bodyParser = require("body-parser"),
  app = express(),
  helmet = require("helmet");
// common = require('./common'),
// config = common.config();

app.use(helmet());
// require('dotenv').config();

// console.log(process.argv[2]);

// if (process.argv[2] == "prod") {
//   DEV_DB_URL = "mongodb://localhost:27017/flyt_live"; // Production DB
// } else {
//   DEV_DB_URL = "mongodb://hiteshpr:hitesh12345@ds119663.mlab.com:19663/flyt_live"; // Development DB
// }

// Set up mongoose connection
// DEV_DB_URL = mongodb://localhost:27017/flyt_live
 DEV_DB_URL = "mongodb://dikshakondekar:Sawli0209@ds061288.mlab.com:61288/flyt_live";

const mongoose = require("mongoose");
mongoose.connect(DEV_DB_URL);
// console.log(DEV_DB_URL);

mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));
// mongoose setup completed

// CORS code
app.use(cors());
// CORS ends

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

// Redis Code
// let redis = require("redis");
// let redisClient = redis.createClient(); // create a Redis Client

// redisClient.on("ready", function() {
//   console.log("Redis is ready"); // If redis successfully started, console log this
// });

// redisClient.on("error", function() {
//   console.log("Error in Redis"); //     if redis successfully started, console log this
// });

// import routes and JWT library
const jwt = require("jsonwebtoken"),
  auth = require("./auth"),
  jwtRoute = require("./routes/jwt.route");
  missionRoute = require("./routes/mission_details.route");
 

// define routes that do not require authentication here
app.use("/jwt", jwtRoute);


/* middleware to test authentication of the user. */
app.use((req, res, next) => {
  // check header or url parameters or post parameters for token
  var header = JSON.parse(req.headers["authorization"]);
  var token = header.token;

  if (token) {
    //Decode the token
    jwt.verify(token, auth.secretCode, (err, decod) => {
      if (err) {
        res.status(403).json();
      } else {
        //If decoded then call next() so that respective route is called.
        res.locals.username = decod.username;
        res.locals.password = decod.password;

        //console.log(res.locals.emailId);

        // if(res.locals.emailId==""){
        //   res.status(403).json({});
        // }
        next();
      }
    });
  } else {
    res.status(403).json({
      message: "No Token"
    });
  }
});


/* middleware to test authentication ends */

app.use("/mission", missionRoute);




// post on which node server would run.
const port = 5555;

app.listen(port, () => {
  console.log("Server is up and running on port number " + port);
});

