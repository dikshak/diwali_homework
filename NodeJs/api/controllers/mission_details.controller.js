
const v = require('node-input-validator');

exports.solve_mission = function (req, res) {
   // console.log(res.body);

       var ci =req.body.ci;
       var zl =req.body.zl;
       var vi =req.body.vi;
       var la =req.body.la;
       var tl =req.body.tl;
       var hl =req.body.hl;
       var ea =req.body.ea;
       var er =req.body.er;
       var mb =req.body.mb;
       var md =req.body.md;
       var vs =req.body.vs;
       var rs =req.body.rs;
       var ff =req.body.ff;
       var mw =req.body.mw;
       var mv =req.body.mv;
       var dl =req.body.dl;
       var wt =req.body.wt;
       var ct =req.body.ct;
       var bw =req.body.bw;

        
       let validator = new v(
           {
            ci:ci,
            zl:zl,
            vi:vi,
            la:la,
            tl:tl,
            hl:hl,
            ea:ea,
            er:er,
            mb:mb,
            md:md,
            vs:vs,
            rs:rs,
            ff:ff,
            mw:mw,
            mv:mv,
            dl:dl,
            wt:wt,
            ct:ct,
            bw:bw
            },
        {
           ci:'required|string',
           zl:'required|integer',
           vi:'required|string',
           la:'required',
           tl:'required|object',
           hl:'required|object',
           ea:'required',
           er:'required|integer',
           mb:'required|integer',
           md:'required|integer',
           vs:'required|string',
           rs:'required',
           ff:'required',
           mw:'required',
           mv:'required',
           dl:'required|integer',
           wt:'required|integer',
           ct:'required|integer',
           bw:'required'
        });
        console.log('hello');
        
        validator.check().then(function (matched) {
           // console.log(matched);
            if(matched===false){
                console.log(validator.errors);
                res.json({
                    'su': false, 'message':validator.errors
                });
               }
               else{
                if(mb<=20){
                    var result ={
                        "su": false,
                        "mi": null,
                        "bf": null,
                        "wp": null,
                        "ex": null,
                        "re": "Battery level below required"
                        }
                        
                    res.status(200).json(result);
                   }else{

                   
                   if(mb==0){
                    res.status(200).json({
                       "su": false, "message": "battery level check is disabled,"
                      });
                   }else{
                   if(vi!="abcxyz123"){
                    res.status(404).json();
                   }
                   else{
                    var result = {
                        "su": true,
                        "mi": "56bbdb17",
                        "bf": {
                          "airspace": {
                            "color": "orange",
                            "advisories": [
                              {
                                "id": "4684250",
                                "name": "HAWTHORNE CLASS D requires FAA authorization, permissible below 400 ft",
                                "last_updated": null,
                                "latitude": 33.8583459,
                                "longitude": -118.375023,
                                "distance": 0,
                                "type": "controlled_airspace",
                                "city": "Redondo Beach",
                                "state": "California",
                                "country": "USA",
                                "rule_id": 5069,
                                "ruleset_id": "usa_part_107",
                                "properties": {
                                  "url": "https://www.faa.gov/uas/request_waiver/",
                                  "ceiling": 400,
                                  "airport_id": "HHR",
                                  "airport_name": "Jack Northrop Fld/Hawthorne Muni",
                                  "airspace_classification": "D",
                                  "type": "d"
                                },
                                "color": "orange",
                                "requirements": {
                                  "notice": {
                                    "phone": null,
                                    "digital": false
                                  }
                                }
                              }
                            ]
                          },
                          "rulesets": [
                            {
                              "id": "usa_part_107",
                              "rules": [
                                {
                                  "short_text": "Do not fly in Class D controlled airspace without ATC authorization",
                                  "description": "Operations in Class B, C, D and E airspace are allowed with the required ATC permission. ",
                                  "status": "conflicting",
                                  "flight_features": [
                                    {
                                      "flight_feature": "flight_geo_intersect",
                                      "status": "conflicting",
                                      "description": "your flight is within Class D airspace",
                                      "input_type": null,
                                      "measurement_type": null,
                                      "measurement_unit": null
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        "wp": [
                          {
                            "frame": 3,
                            "command": 16,
                            "is_current": true,
                            "autocontinue": true,
                            "param1": 6,
                            "param2": 7,
                            "param3": 0,
                            "param4": 0,
                            "x_lat": 65.425532,
                            "y_long": 18.542422,
                            "z_alt": 25
                          },
                          {
                            "frame": 3,
                            "command": 16,
                            "is_current": true,
                            "autocontinue": true,
                            "param1": 6,
                            "param2": 7,
                            "param3": 0,
                            "param4": 0,
                            "x_lat": 65.425552,
                            "y_long": 18.542442,
                            "z_alt": 25
                          }
                        ],
                        "ex": 150,
                        "re": null
                      };
                    res.json(result);
                       
                   }
                   
               }
            }
        }
          //  console.log(validator.errors);
        });      


    
};


exports.begin_mission = function (req, res) {
    // console.log(res.body);
 
        var mi =req.body.mi;
                
        let validator = new v(
            {
                 mi:mi
             },
         {
            mi:'required|string',
           
         });
         //console.log('hello');
         
         validator.check().then(function (matched) {
            // console.log(matched);
             if(matched===false){
                 console.log(validator.errors);
                 res.json({
                     'su': false, 'message':validator.errors
                 });
                }
                else{
                 if(mi!="56bbdb17"){
                     res.status(404).json();
                    }
                    else{
                     var result = {
                         "su": true,
                         "re": null
                       };
                     res.json(result);
                        
                    }
                }
                    
           //  console.log(validator.errors);
         });      
    
 };


 exports.get_mission_status = function (req, res) {
    // console.log(res.body);
 
        var mi =req.body.mi;
                
        let validator = new v(
            {
                 mi:mi
             },
         {
            mi:'required|string',
           
         });
         //console.log('hello');
         
         validator.check().then(function (matched) {
            // console.log(matched);
             if(matched===false){
                 console.log(validator.errors);
                 res.json({
                     'su': false, 'message':validator.errors
                 });
                }
                else{
                 if(mi!="56bbdb17"){
                     res.status(404).json();
                    }
                    else{
                     var result = {
                         "st": "Under Execution",
                         "vs": "Left for home"
                       };
                     res.json(result);
                        
                    }
                }
                    
           //  console.log(validator.errors);
         });      
    
 };

 exports.con_payload = function (req, res) {
  // console.log(res.body);

        var mi =req.body.mi;
        var ci =req.body.ci;
        var zl =req.body.zl;
        var gr =req.body.gr;
        var gp =req.body.gp;
        var gy =req.body.gy;


              
      let validator = new v(
          {
            mi:mi,
            ci:ci,
            zl:zl,
            gr:gr,
            gp:gp,
            gy:gy

           },
       {
        mi:'required|string',
        ci:'required|string',
        zl:'required|integer',
        gr:'required',
        gp:'required',
        gy:'required'

         
       });
       //console.log('hello');
       
       validator.check().then(function (matched) {
        // console.log(matched);
         if(matched===false){
             console.log(validator.errors);
             res.json({
                 'su': false, 'message':validator.errors
             });
            }
            else{
             if(mi!="56bbdb17"){
                 res.status(404).json();
                }
                else{
                  if(ci!="ABC1"){
                        var result = {
                          "su": false,
                          "re": "Camera Id invalid"
                        };
                      res.json(result);
                   }
                   else{
                      var result = {
                          "su": true,
                          "re": null
                        };
                      res.json(result);
                    
                }
              }
            }
                
       //  console.log(validator.errors);
     });    
  
};



