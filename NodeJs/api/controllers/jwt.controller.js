

const jwt = require('jsonwebtoken'),
auth = require('../auth'),
v = require('node-input-validator');

exports.get_jwt_token = function (req, res) {
    //console.log(req.body);  
       var username =req.body.username;
       var password =req.body.password;

       let validator = new v({username:username,password:password}, {username:'required|String',password:'required|String'});
        
        validator.check().then(function (matched) {
           // console.log(matched);
            if(matched===false){
                res.json({
                    'success': false, 'message':validator.errors
                });
               }
               else{

                    if(username=='admin' && password=='admin'){
                        var token = generateJwtToken(username,password);
                        res.json(token);
                    }
                    else{
                        res.json({
                            'success': false, 'message':'Invalid credentials'
                        });
                    }
                    
               }
          //  console.log(validator.errors);
        });
       
      // var checkMail= ValidateEmail(email);
       //console.log(checkMail);
      
          
};


// send the secret code needed to decrypt JWT. Code is BASE64 encrypted.
exports.send_secret_code = function (req, res){

    res.json(Buffer.from(auth.secretCode).toString('base64'));

}

// // generate the new JWT token
// exports.generate_new_token = function (req, res){

//     var header = JSON.parse(req.headers['authorization']),
//      token = header.token,
//      refreshToken = header.refreshToken,
//      data = jwt.decode(token,auth.secretCode),
//      userId = data.id,
//      userEmail = data.email;
//      isAdmin = data.isAdmin;

//     client.get(refreshToken,function(err,reply) {

//     var idFromRedis = reply;

//     if(idFromRedis === userId){

//         var token  = generateJwtToken(userId, userEmail, isAdmin);

//             res.json(token);

//         } else {
//             res.status(404);
//             res.json({
//                 'status': 404, 'message':'No user found'
//               });
//         }
//     });
// }

function generateJwtToken(username,password){

    var userDat = {
        'username': username,
        'password': password
        };

    var token = jwt.sign(userDat, auth.secretCode , { expiresIn: 1800 });           // Expires in 30 mins
   
    return {'token': token};

}

function ValidateEmail(email) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
    return (true)
  }
     return (false)
}

