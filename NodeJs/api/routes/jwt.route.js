
const express = require('express');
const router = express.Router();

const jwt_controller = require('../controllers/jwt.controller');


router.post('/getJwtToken', jwt_controller.get_jwt_token);                  // uget JWT Token


module.exports = router;
