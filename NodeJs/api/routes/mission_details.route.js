
const express = require('express');
const router = express.Router();

const mission_details_controller = require('../controllers/mission_details.controller');


router.post('/solve_mission', mission_details_controller.solve_mission);  
router.post('/begin_mission', mission_details_controller.begin_mission); 
router.post('/get_mission_status', mission_details_controller.get_mission_status);   
router.post('/control_payload', mission_details_controller.con_payload);   

           





module.exports = router;
